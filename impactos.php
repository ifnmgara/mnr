<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>Reciclagem Eletrônica</title>
<?php include "links.php"; ?>
</head>
<body>
<?php include "headerimpactos.php"; ?>
<div class="row">   
  <?php include "submenu_home.php"; ?>
  <div class="col-6 col-s-9">




<div class=text-aling center >
<h2>Impactos do Lixo Eletrônico</h2>
  <h3>Extração de Recursos Naturais:</h3> <p> A produção de componentes eletrônicos requer a extração de uma variedade de recursos naturais, incluindo metais como ouro, prata, cobre, e metais de terras raras. A mineração desses recursos pode levar à degradação do solo, contaminação da água e perda de habitat.</p>
  <h3>Poluição do Ar e da Água:</h3><p> Os processos de fabricação de componentes eletrônicos muitas vezes envolvem o uso de substâncias químicas tóxicas, como ácidos, solventes e metais pesados. Essas substâncias podem poluir o ar e a água, causando danos à saúde humana e ao meio ambiente.</p>
  <h3>Descarte de Resíduos Eletrônicos:</h3><p> A rápida obsolescência dos dispositivos eletrônicos leva a um grande volume de resíduos eletrônicos descartados a cada ano. Muitos desses resíduos contêm substâncias perigosas e não são adequadamente reciclados, o que pode resultar em contaminação do solo e da água.</p>
  <h3>Consumo de Energia:</h3> <p>A produção e o uso de dispositivos eletrônicos consomem uma quantidade significativa de energia, especialmente aqueles que requerem alto desempenho ou têm grande demanda por recursos de computação, como servidores de data center. Isso pode contribuir para as emissões de gases de efeito estufa e o aquecimento global.</p>
  <h3>Esgotamento de Recursos Não Renováveis:</h3><p>A demanda crescente por dispositivos eletrônicos está levando ao esgotamento de recursos não renováveis, como metais preciosos e terras raras. Isso pode levar a escassez futura e aumentar os custos de produção.</p>
  <h3>Impactos Sociais:</h3><p> A mineração de metais utilizados na produção de componentes eletrônicos muitas vezes está associada a violações dos direitos humanos e condições de trabalho precárias, especialmente em países em desenvolvimento onde as regulamentações são menos rigorosas.</p>
  <h3>Desafios de Reciclagem:</h3><p> Embora a reciclagem de eletrônicos seja importante para reduzir o impacto ambiental, ela enfrenta desafios significativos, como a complexidade dos produtos eletrônicos, a falta de infraestrutura de reciclagem adequada e o custo envolvido.</p>
  <h3>Tecnologias Sustentáveis:</h3><p> Há uma crescente conscientização sobre a necessidade de desenvolver tecnologias mais sustentáveis na indústria eletrônica, incluindo o uso de materiais recicláveis, a redução do consumo de energia e a extensão da vida útil dos produtos. </p>
  <p>Esses são alguns dos principais impactos ambientais associados aos componentes eletrônicos. Abordar esses problemas requer uma abordagem holística que envolva governos, indústrias e consumidores na adoção de práticas mais sustentáveis ao longo de todo o ciclo de vida dos produtos eletrônicos.</p>

</div>







</div> 
</div>
<?php include "footer.php"; ?>
</body>
</html>
