<div class="col-6 col-s-9">
    <h1>Propósito</h1>
    <p>O Brasil é o quinto maior produtor de lixo eletrônico do mundo, e o descarte incorreto desse tipo de resíduo vem aumentando significativamente nos últimos anos. Isso gera dois grandes problemas: impactos ambientais de grande escala e desperdício de componentes que poderiam ser reutilizados na construção de novos equipamentos eletrônicos. Levando em consideração esses desafios, especialmente na região de Araçuaí-MG, onde as iniciativas de reciclagem e redução do impacto do lixo eletrônico são escassas, surge a necessidade urgente de ações concretas.</p>
<p> Pensando na questão ambiental e econômica, as alunas do 2° ano do curso Técnico em Informática do IFNMG campus Araçuaí, sob a orientação do professor Jeancarlo Campos Leão, desenvolveram um projeto de robótica focado no reaproveitamento de lixo eletrônico para a construção de novos dispositivos, inclusive robôs, que auxiliem em tarefas do cotidiano. O projeto visa não apenas reduzir os impactos ambientais, mas também economizar custos de aquisição de novos componentes para o laboratório Kiau Maker.</p>
<p>A equipe Meninas na Robótica do IFNMG campus Araçuaí reconheceu o aumento significativo de resíduos eletrônicos no país e decidiu promover uma ação ativa e engajada, incentivando a reciclagem desses materiais no campus. Na região, a falta de pontos de coleta que assegurem uma destinação final ambientalmente adequada, incluindo a reciclagem, é um desafio. Assim, o projeto não se limita ao recolhimento de resíduos para desmontagem e reaproveitamento de peças, mas também foca na reciclagem desses materiais, criando novos protótipos a partir do que é considerado "lixo".
 </p>
 <p>O projeto teve continuidade pelo grupo Meninas na Robótica no 3° ano do curso Técnico em Informática, como projeto premiado pela Mostra Nacional de Robótica (MNR) em 2023. 
</p>
</div>




