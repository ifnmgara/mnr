<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>Reciclagem Eletrônica</title>
<?php include "links.php"; ?>
</head>
<body>
<?php include "headercomponentes.php"; ?>
<div class="row">   
  <?php include "submenu_riscos.php"; ?>
<div class="col-6 col-s-9">
<h1>Componentes Eletrônicos</h1>

<h3>Capacitores: </h3>
<p>Os capacitores são componentes eletrônicos que armazenam energia elétrica. Eles geralmente contêm materiais como alumínio, tântalo ou polímeros condutores. Os impactos ambientais associados aos capacitores incluem:
- Mineração de metais: Muitos capacitores contêm materiais como alumínio ou tântalo, cuja extração pode causar danos ambientais, incluindo degradação do solo e contaminação da água.
-Uso de substâncias tóxicas: Alguns capacitores podem conter substâncias químicas perigosas, como bifenilos policlorados (PCBs) ou retardadores de chama bromados (BFRs), que representam riscos de poluição e impactos na saúde humana.
-Descarte inadequado: Se não forem reciclados corretamente, os capacitores podem liberar essas substâncias tóxicas no meio ambiente, contribuindo para a poluição do solo e da água.
</p>
<img src="https://storage.googleapis.com/novo-blog-wordpress/2021/05/exemplos-de-capacitores-1024x683.jpg"  style="width:50%;" class="center">
<h3>Resistores:</h3>
<p>Os resistores são componentes eletrônicos que limitam o fluxo de corrente elétrica em um circuito. Eles são frequentemente feitos de materiais como carbono ou metais. Os impactos ambientais dos resistores incluem:
-Consumo de recursos naturais: A fabricação de resistores pode exigir a extração de metais como cobre ou níquel, contribuindo para a depleção de recursos naturais não renováveis.
-Consumo de energia: Os processos de fabricação de resistores consomem energia, muitas vezes proveniente de fontes não renováveis, o que contribui para as emissões de gases de efeito estufa e o aquecimento global.
-Emissões de poluentes: A produção de resistores pode gerar emissões de poluentes atmosféricos e resíduos sólidos, contribuindo para a poluição do ar e do solo.
</p>
<img src="https://s1.static.brasilescola.uol.com.br/be/2020/02/resistores-4.jpg"  style="width:50%;" class="center"">

<h3>Transistores:</h3>
<p>Os transistores são dispositivos semicondutores usados para amplificar ou comutar sinais elétricos. Eles são feitos de materiais semicondutores, como silício ou germânio. Os impactos ambientais dos transistores podem incluir:
-Uso intensivo de energia: A fabricação de transistores requer processos de fabricação complexos e intensivos em energia, que podem contribuir para as emissões de gases de efeito estufa.
-Uso de materiais perigosos: Alguns transistores podem conter materiais perigosos, como metais pesados ou produtos químicos tóxicos, que representam riscos para o meio ambiente e a saúde humana.
-Descarte de resíduos eletrônicos: Como muitos componentes eletrônicos, os transistores podem acabar em aterros sanitários se não forem reciclados adequadamente, contribuindo para a acumulação de resíduos eletrônicos e a contaminação ambiental.
</p>
<img src="https://www.makerhero.com/wp-content/uploads/2023/02/image6.png"  style="width:50%;" class="center">

<h3>Diodos:</h3>
<p>Os diodos são dispositivos eletrônicos que permitem o fluxo de corrente em apenas uma direção. Eles são feitos de materiais semicondutores, como silício ou germânio. Os impactos ambientais dos diodos são semelhantes aos dos transistores, incluindo o uso intensivo de energia na fabricação e o potencial uso de materiais perigosos.
Em geral, os componentes eletrônicos têm uma variedade de impactos ambientais, desde a extração de matérias-primas até o descarte final. A adoção de práticas de fabricação e reciclagem sustentáveis é essencial para mitigar esses impactos e promover uma indústria eletrônica mais ambientalmente responsável.
</p>

<img src="https://electronicaonline.net/wp-content/uploads/2021/06/tipos-de-diodos.jpg "  style="width:50%;" class="center">

</div> </div>
<?php include "footer.php"; ?>
</body>
</html>
