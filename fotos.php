<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>Reciclagem Eletrônica</title>
<?php include "links.php"; ?>
</head>
<body>
<?php include "headerresultados.php"; ?>
<div style="text-align:center"> 
<div class="row">   
  
<h2>Registros das atividades</h2>

<h3>Segue abaixo registros de algumas atividades realizadas pela equipe Meninas na Robótica.</h3>

<img src="imgs/1.jpeg"  style="width:60%;" class="center">
<h3>Apresentação do esboço da plataforma web para alunos do curso Técnico em Informática do IFNMG-Campus Araçuaí</h3>
<img src="imgs/2.jpeg"  style="width:60%;" class="center">
<h3>Capacitação dos estudantes do projeto: Extração de componentes eletrônicos em máquinas que foram descartadas para reutilização na robótica</h3>
<img src="imgs/mnr1.jpeg"  style="width:60%;" class="center">
<img src="imgs/mnr2.jpeg"  style="width:60%;" class="center">
<img src="imgs/mnr3.jpg"  style="width:60%;" class="center">
<h3>Apresentação do projeto na Mostra Nacional de Robótica (MNR) 2023 </h3>
<img src="imgs/3.jpeg"  style="width:60%;" class="center">
<img src="imgs/4.jpeg"  style="width:60%;" class="center">
<h3>Capacitação: Uso de impressora 3D na robótica</h3>
</div>

</div>
<?php include "footer.php"; ?>
</body>
</html>