<div class="footer">
<link rel="stylesheet" href="equipe.css"> 
<div class="container-footer">
<div class="container">    
    <div class="left">
        <div class="titulo-footer">Reciclagem Eletrônica</div>
        <div class="titulo-footer">Gerenciamento do lixo eletrônico - IFNMG-Campus Araçuaí</div>
        <div class="titulo-footer">E-mail para contato:</div>
        <li><a href="https://mail.google.com/">plataformawebparagerenciamento@gmail.com</a></li>
    </div>
    <div class="center">
    </div>
    <p></p>
        <div class="titulo-footer">Saiba mais sobre o nosso projeto em:</div>
        <ul>
            <li><a href="equipe.php">Membros do projeto</a></li>
            <li><a href="index.php">Objetivo do projeto</a></li>
        </ul>
    </div>


<!--nav id="rodape"> 
  <ul> 
    <li><a href="#">Home</a></li>
    <li><a href="#">Produtos</a></li>
    <li><a href="#">Missão</a></li>
    <li><a href="#">Links</a></li>
    <li><a href="#">Contato</a></li>
  </ul> 
</nav-->
<div class="social">
  <!-- Add font awesome icons -->
  
</head>
<body>
    <div class="container">
        <div class="thank-you">Obrigado por visitar nosso site!</div>
        <div class="message"> Fique à vontade para nos seguir nas redes sociais e se inscrever.</div>
        
        <div class="social-media">
            <a href="https://www.instagram.com/meninas.na_robotica?igsh=MXkyaTQ5dGF3YWtjOA==" target="_blank">Instagram</a> 
            <a href="https://www.instagram.com/meninas.na_robotica?igsh=MXkyaTQ5dGF3YWtjOA==" class="fa fa-instagram"></a>
            <a href="https://www.youtube.com/@clubedarobotica4649" target="_blank">Youtube</a> 
            <a href="https://www.youtube.com/@clubedarobotica4649" class="fa fa-youtube"></a>        
        </div>
        
        <div class="footer">
            &copy; 2024. Todos os direitos reservados.
        </div>
    </div>
</body>
</html>