<h2>Membros do projeto</h2>
</div>
<section>
<div class="divisor">
<div class="cards">
<div class="card">
<div class="card-content">
<div class="image">
<img src="imgs/camilab.jpeg" alt="">
</div>
<div class="media-icons">
<a href="https://instagram.com/ca.barbosachaves?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D" target="_blank"><i class="fab fa-instagram"></i></a>
<a href="http://lattes.cnpq.br/3859173452372861 " target="_blank" title="Curriculo Lattes" rel="noopener noreferrer"><i class="fas fa-university"></i></a>
</div>
<div class="name-profession">
<span class="name">Camila Barbosa Chaves</span>
<span class="profession">Estudante-Técnico em Informática-Desenvolvimento Web</span>
</div>
</div>
</div>
<div class="card">
<div class="card-content">
<div class="image">
<img src="imgs/lorrainy.jpeg" alt="">
</div>
<div class="media-icons">
<a href="https://instagram.com/lorrainy__campos?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D" target="_blank"><i class="fab fa-instagram"></i></a>
<a href="http://lattes.cnpq.br/3063011012116382" target="_blank" title="Curriculo Lattes" rel="noopener noreferrer"><i class="fas fa-university"></i></a>
</div>
<div class="name-profession">
<span class="name">Lorrainy Campos</span>
<span class="profession">Estudante-Técnico em Informática-Pesquisas</span>
</div>
</div>
</div>
<div class="card">
<div class="card-content">
<div class="image">
<img src="imgs/marina.jpeg" alt="">
</div>
<div class="media-icons">
<a href="https://instagram.com/a.ninacosta?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D" target="_blank"><i class="fab fa-instagram"></i></a>
<a href="http://lattes.cnpq.br/1476207148264912" target="_blank" title="Curriculo Lattes" rel="noopener noreferrer"><i class="fas fa-university"></i></a>
</div>
<div class="name-profession">
<span class="name">Marina Costa</span>
<span class="profession">Estudante-Técnico em Informática-Pesquisas</span>
</div>
</div>
</div>
<div class="card">
<div class="card-content">
<div class="image">
<img src="imgs/giovanna.jpeg" alt="">
</div>
<div class="media-icons">
<a href="https://instagram.com/a.ninacosta?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D" target="_blank"><i class="fab fa-instagram"></i></a>
<a href="https://github.com/marcosVSpereira" target="_blank" title="Github" rel="noopener noreferrer"><i class="fab fa-github"></i></a>
<a href="http://lattes.cnpq.br/5270383203851900" target="_blank" title="Curriculo Lattes" rel="noopener noreferrer"><i class="fas fa-university"></i></a>
</div>
<div class="name-profession">
<span class="name">Giovanna Martins</span>
<span class="profession">Estudante-Técnico em Informática-Pesquisas</span>
</div>
</div>
</div>
<div class="card">
<div class="card-content">
<div class="image">
<img src="imgs/anacristina.jpeg" alt="">
</div>
<div class="media-icons">
<a href="https://instagram.com/acris_cs?utm_source=qr&igshid=ZDc4ODBmNjlmNQ%3D%3D" target="_blank" title="Instagram" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
<a href="https://github.com/MatheusAlvesPereiraRosa" target="_blank" title="Github" rel="noopener noreferrer"><i class="fab fa-github"></i></a>
<a href="http://lattes.cnpq.br/9563293893133901" target="_blank" title="Curriculo Lattes" rel="noopener noreferrer"><i class="fas fa-university"></i></a>
</div>
<div class="name-profession">
<span class="name">Ana Cristina Cunha </span>
<span class="profession">Estudante-Técino em Informática-Pesquisas</span>
</div>
</div>
</div>
<div class="card">
<div class="card-content">
<div class="image">
<img src="http://servicosweb.cnpq.br/wspessoa/servletrecuperafoto?tipo=1&id=K4296014A6" alt="">
</div>
<div class="media-icons">
<a href="http://lattes.cnpq.br/0451966994155112" target="_blank" title="Curriculo Lattes" rel="noopener noreferrer"><i class="fas fa-university"></i></a>
</div>
<div class="name-profession">
<span class="name">Jeancarlo Campos</span>
<span class="profession">Coordenador do Projeto</span>
</div>
</div>
</div>
</section>
