<div class="header">
  <h1><a href="index.php"><img class="logo" src="imgs/logo2.0.jpeg"></a>Reciclagem Eletrônica</h1>
  <?php include "menu.php"; ?>
</div>
<link rel="stylesheet" href="center.css">  
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="imgs/apoio.png" alt="apoio" style="width:100%;" class="center">
      </div>

      <div class="item">
        <img src="imgs/banner3.png" alt="banner3" style="width:100%;" class="center">
      </div>

      <div class="item" >
        <img src="imgs/banner4.png" alt="banner4" style="width:100%;" class="center">
      </div>
    
      <div class="item">
        <img src="imgs/banner1.png" alt="banner" style="width:100%;" class="center">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>