<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>Reciclagem Eletrônica</title>
<?php include "links.php"; ?>
</head>
<body>
<?php include "headerresultados.php"; ?>
<div class="text-align:center">
<div class="row">   
<?php include "submenu_resultados.php"; ?>
  <div class="col-6 col-s-9">
<h1>Resultados</h1>

<p>A plataforma REL (Reutilização de Lixo Eletrônico) exemplifica   a integração eficaz entre sustentabilidade, educação e inovação para enfrentar o desafio do descarte inadequado de lixo eletrônico. Além de promover a conscientização sobre o descarte responsável e a reciclagem criativa de componentes eletrônicos.</p>
<p>Com a premiação na Mostra Nacional de Robótica (MNR) 2023, nosso trabalho ganhou continuidade no desenvolvimento pelo grupo Meninas na Robótica. </p>
<p>Na página -Atividades- você poderá encontrar registros das atividades e apresentações realizadas pela equipe Meninas na Robótica durante o projeto.</p>

</div>
</div>
</div>
<?php include "footer.php"; ?>
</body>
</html>